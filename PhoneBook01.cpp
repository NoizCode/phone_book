#include <iostream>
#include <string>
#include <map>
#include <cstdlib>
#include <iterator>
using namespace std;


class myClass {
public:
	void setName(string x) {
		name = x;
	}
	string getName() {
		return name;
	}
public:
	string name;
};


int name_and_address()
{
	/*address_book.insert(make_pair(first_name, make_pair(middle_name, last_name))); << use this template to add stuff to map object
	https://stackoverflow.com/questions/14070940/c-printing-out-map-values */
	std::map <string, string> address_book;
	std::cout << "You picked number 1" << "\n" << endl; // debug line ******
	
	string full_Name;
	string address;
	std::cout << "Enter your full name\n" << endl;
	std::cout << ">> ";
	std::getline(cin, full_Name);
	std::cin.ignore(1000, '\n');
	std::cout << "\n" << endl;
	
	//std::cout.flush();

	std::cout << "Enter your address\n" << endl;
	std::cout << ">> ";
	std::getline(cin, address);
	std::cout << "\n" << endl;



	//std::map <string, string> address_book; // defines the key/value pair and assigns it to the address_book
	//address_book["Alan"] = "53 Howard St";
	//std::cout << address_book["Alan"];
	//std::cout << endl;
	//std::cout << "\nCurrent address book is : ";// << address_book;



	return 0;
}

int list_all()
{
	std::cout << "You picked number 2"  << endl; // debug line ******
	return 0;
}

void quit_func()
{
	std::cout << "You chose to quit.......goodbye" << "\n" << endl; // debug line ******
	system("pause");
	exit(1);
}
int incorrect_choice()
{
	std::cout << "That was an incorrect choice.\n \nPlease try agian.\n" << endl;
	return 0;
}


int menu()
{
	// declare function variables
	int menu_Choice;
	menu_Choice = 0;
	std::cout << "Welcome to the address book" << endl << "\n";
	std::cout << "Please make a selection" << endl << "\n";

	// Loop through to call function based on user input
	while (menu_Choice == 0)
	{
		// *********** Give menu choices ***********
		
		std::cout << "\n1. Enter name and address" << endl;
		std::cout << "2. List all current Names along with their addresses" << endl;
		std::cout << "3. Exit the program" << endl << "\n";
		std::cout << ">> ";
		std::cin >> menu_Choice;
		//std::cout << "\nYou chose: " << menu_Choice << "\n" << endl; // debug line **********
		// ******** Menu choice end*************
		// try using switch case in here instead of the if statements*******************
		
		/*while (menu_Choice == 0)*/
		{
			switch (menu_Choice)
			{
			case 1:
				name_and_address();
				break;
			case 2:
				list_all();
				break;
			case 3:
				quit_func();
				break;
			default:
				std::cout.flush(); // this flushes out anything assigned to menu_Choice to allow for default to occur
				incorrect_choice();
				break;
				//menu_Choice = 0;
			}
		}
		menu_Choice = 0;
		
		// *** the If statements that work for menu choices 
		//if (menu_Choice == 1)
		//{
		//	name_and_address();
		//}
		//if (menu_Choice == 2)
		//{
		//	list_all();
		//}
		//if (menu_Choice == 3)
		//{
		//	quit_func();
		//}
		//else (menu_Choice == 0);
		//{
		//	menu_Choice = 0;
		//}


	}
	



	return menu_Choice;

}


int main() // working dictionary, known in C++ as STL(Standard Template Library) - https://www.cprogramming.com/tutorial/stl/stlmap.html
{
	std::cout << menu();
	

	std::map <string, string> address_book; // defines the key/value pair and assigns it to the address_book
	address_book["Alan"] = "53 Howard St";
	std::cout << address_book["Alan"];
	std::cout << endl;

	system("pause");
	return 0;
}



//
//class credentials
//{
//public:
//	string name;
//};
//
//string phoneNumber(string pNumber)
//{
//	string pNumb;
//	std::cout << "What is your Phone Number" << endl;
//	std::getline(cin, pNumb);
//	std::cout << "\n" << "The number you entered is: " << endl;
//	std::cout << pNumb;
//	return pNumb;
//
//
//}
//
//
//int main()
//{
//	{
//		credentials myName;
//		myName.name = "Your Name";
//		cout << myName.name << endl;
//		
//	}
//	std::cout << "Enter your name: " << endl;
//	string fullName;
//	std::getline(cin, fullName);
//	std::cout << "The name you entered is : ";
//	std::cout << fullName << endl;
//	std::cout << endl;
//	
//	std::cout << "Enter your address: " << endl;
//	string address;
//	std::getline(cin,address);
//	std::cout << "The address you entered is : " << endl;
//	std::cout << address << endl;
//	std::cout << endl;
//
//	string pNumber;
//	phoneNumber(pNumber);
//	std:cout << pNumber << endl;
//
/*
string myName;
std::cout << "What is your name: ";
std::getline(cin, myName);

myClass myObj;
myObj.setName(myName);
cout << "\n" << myObj.getName() << endl; */
//	
//
//	cout << endl;
//	system("pause"); // ending main program. Prints "Press any key to continue...."
//	return 0;
//}